$(function () {
    $('#form_create_task').on('submit', function (e) {
        console.log('creating task');
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: 'index.php',
            data: $('#form_create_task').serialize(),
            success: function (data) {
                $('#task_list').html(data);
            }
        });
    });
    $('span.delete_task').on('click', function (e) {
        var id = $(this).attr('data-id');
        var elem = $(this);
        console.log('deleting ' + id);
        $.ajax({
            type: 'post',
            url: 'index.php',
            data: {
                target: 'task',
                action: 'delete',
                id: id,
            },
            success: function (data) {
                //$('#project_list').html(data);
                elem.parent('div.list-group-item').fadeOut();
            }
        });
    });
})
