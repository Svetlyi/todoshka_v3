<h1>Tasks</h1>
<form class="form" method='POST' id='form_create_task'>
  <div class="form-group">
    <label class="sr-only" for="new_task">New task</label>
    <input type="hidden" name="action" value="create"/>
    <input type="hidden" name="target" value="task"/>
    <input type="text" class="form-control" id="new_task" name="name" placeholder="Enter new task">
  </div>
</form>
<div class="list-group" id='task_list'>
    <?php include('list.php');?>
</div>
