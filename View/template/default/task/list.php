<a href="#" class="list-group-item active">
    Selected task
</a>
<?php foreach ($tasks as $task): ?>
    <div class="list-group-item"><?= $task['name']?><span data-id='<?= $task['id']?>' class="delete_task glyphicon glyphicon-remove"></span></div>
<?php endforeach ?>
