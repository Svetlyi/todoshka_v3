</div>
</div>
<div>Footer</div>
<script type="text/template" id="error-template">
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <%= error %>
</div>
</script>
<script type="text/template" id="success-template">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <%= error %>
</script>

<script type="text/javascript" src="./View/template/default/js/jquery.min.js"></script>
<script type="text/javascript" src="./View/template/default/js/bootstrap.min.js"></script>
<script text="text/javascript" src="./View/template/default/js/underscore-min.js"></script>
<script type="text/javascript" src="./View/template/default/js/backbone-min.js"></script>
<script type="text/javascript" src="./View/template/default/js/backbone.syphon.min.js"></script>

<script type="text/javascript" src="./View/template/default/js/errorHandler.js"></script>

<?php foreach ($scripts as $script):?>
<script src="<?=$script?>"></script>
<?php endforeach; ?>
</body>
</html>
