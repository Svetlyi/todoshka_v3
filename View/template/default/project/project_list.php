<div class="col-md-3">
    <h2>Projects</h2>
    <a href="#" class="inbox list-group-item active">
        Inbox
    </a>
    <div class="list-group" id='project_list'>
        <?php require('list.php'); ?>
    </div>
    <form class="form" method='POST' id='form_create_project'>
      <div class="form-group">
        <label class="sr-only" for="new_project">New project</label>
        <input type="hidden" name="action" value="create"/>
        <input type="hidden" name="target" value="project"/>
        <input type="text" class="form-control" id="new_project" name="name" placeholder="Enter new project">
      </div>
    </form>
</div>
<script type="text/template" id="project-template">
    <div class="view">
        <% for(var i=0;i<depth;i++) { %>
        <div class="delimetr"><span class="glyphicon glyphicon-minus"></span></div> 
        <% } %>
        <%= name %>

        <div class="tools">
            <span project-id="<%= id %>" class="add-sub-project glyphicon glyphicon-folder-open"></span>
            <span project-id="<%= id %>" class="delete-project glyphicon glyphicon-remove"></span>
        </div>
    </div>

    <input type="text" value="<%= name %>" class="edit-project form-control"/>
    <input type="text" class="add-sub-project form-control"/>
</script>
