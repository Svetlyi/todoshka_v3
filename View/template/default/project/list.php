<?php foreach ($projects as $project): ?>
    <a class="list-group-item">
        <?php for ($i=0;$i<$project['depth'];$i++): ?>
        <div class="delimetr"><span class="glyphicon glyphicon-minus"></span></div> 
        <?php endfor;?>
        <?= $project['name']?>

        <div class="tools">
            <span project-id="<?=$project['id'];?>" class="add-sub-project glyphicon glyphicon-folder-open"></span>
            <span project-id="<?=$project['id'];?>" class="delete-project glyphicon glyphicon-remove"></span>
        </div>
    </a>
<?php 
    if (isset($project['children'])) {
        $parent_projects = $projects;
        $projects = $project['children'];
        include('list.php');
        $projects = $parent_projects;
    }
?>

<?php endforeach ?>
