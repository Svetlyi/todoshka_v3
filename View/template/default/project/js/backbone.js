var app = app || {};
var ENTER_KEY = 13;

app.Project = Backbone.Model.extend({
    url: 'index.php?api&target=project',
    defaults: {
    },

    initialize: function() {
        console.log('init project');
        if(Array.isArray(this.get('children'))) {
            console.log('init child Collection');
            this.set({children: new app.ProjectChildren(this.get('children'))});
        } else {
            var coll = new app.ProjectChildren();
            this.set({children: coll});
        }
    },

    sync: function (method, model, options) {
        if(method === 'delete') {
            options = options || {};
            options.contentType = 'application/json';
            options.data = JSON.stringify(this.toJSON());
        }

        return Backbone.sync.call(this, method, model, options);
    }
});

app.ProjectChildren = Backbone.Collection.extend({
    url: 'index.php?api&target=project',
    model: app.Project,
});

var ProjectList = Backbone.Collection.extend({
    url: 'index.php?api&target=project',
    model: app.Project,
});

app.Projects = new ProjectList();

app.AppView = Backbone.View.extend({
    el: $('.row'),

    events: {
        'keypress #new_project': 'createProjectOnEnter',
        'click .inbox': 'selectProject',
    },

    initialize: function() {
        this.listenTo(app.Projects, 'select_project', this.selectedProject)
        this.$input = this.$('#new_project');
        this.listenTo(app.Projects, 'add', this.addProject);

        app.Projects.fetch({
            success: function() {
                app.Projects.add(app.Projects.attributes);
                console.log('Projects received:');
                console.log(app.Projects.toJSON());
            },
            error: function() {
                console.log('fetch failed');
            },
        });
    },

    selectProject: function() {
        app.setProject(null);
        $('.inbox').addClass('active');
    },

    selectedProject: function() {
        $('.inbox').removeClass('active');
    },

    addProject: function(project) {
        var view = new app.ProjectView({model: project});
        $('#project_list').append(view.render().el);

        if(project.get('children').length>0) {
            _.each(project.get('children').models, function(children) {
                app.Projects.add(children);
            });
        }
    },

    newAttributes: function() {
        return {
            name: this.$input.val().trim(),
            depth: 0,
        }
    },

    createProjectOnEnter: function(event) {
        if(event.which !== ENTER_KEY) {
            return;
        }
        event.preventDefault();

        //app.Projects.create(this.newAttributes(), {
        var value = this.$input.val();
        app.Projects.create({
            name: value,
        }, {
            success: function(project) {
            },
            wait: true,
        });
        console.log(app.Projects.toJSON());
        this.$input.val('');
    }
});

app.ProjectView = Backbone.View.extend({
    tagName: 'a',

    className: 'list-group-item',

    template: _.template($('#project-template').html()),

    initialize: function() {
        this.listenTo(app.Projects, 'select_project', this.selectedProject)
        console.log(this.model.get('children'));
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
        this.listenTo(this.model.get('children'), 'add', this.addChildren);
    },

    events: {
        'click .delete-project': 'delete',
        'click': 'selectProject',

        'click .add-sub-project': 'showFormAddSubProject',
        'blur .add-sub-project': 'closeFormAddSubProject',
        'keypress input.add-sub-project': 'saveSubProjectOnEnter',

        'dblclick': 'edit',
        'keypress .edit-project': 'updateOnEnter',
        'blur .edit-project': 'close',
    },

    selectProject: function() {
        app.setProject(this.model.get('id'));
    },

    selectedProject: function() {
        console.log('Collection');
        console.log(this);
        if (app.getProject() != this.model.get('id')) {
            this.$el.removeClass('active');
        } else {
            this.$el.addClass('active');
        }
    },

    edit: function() {
        this.$el.addClass('edit');
    },

    showFormAddSubProject: function() {
        this.$el.addClass('form-add-sub-project');
    },

    closeFormAddSubProject: function() {
        var value = this.$childInput.val();
        if (value.length > 0) {
            this.$childInput.val('');

            this.model.get('children').create({
                name: value,
                project_id: this.model.get('id')
            }, {
                success: function (project) {
                },
                wait: true
            });
            this.$el.removeClass('form-add-sub-project');
        }
    },

    saveSubProjectOnEnter: function (e) {
        if(e.keyCode == 13) {
            this.closeFormAddSubProject();
        }
    },

    addChildren: function (project) {
        project.set({children: new app.ProjectChildren(project.get('children'))});
        var view = new app.ProjectView({model: project});
        this.$el.after(view.render().el);
    },

    close: function() {
        var value = this.$input.val();
        this.model.save({name: value});
        this.$el.removeClass("edit");
    },

    updateOnEnter: function(e) {
        if(e.keyCode == 13) {
            this.close()
        }      
    },

    delete: function() {
        this.model.destroy({
            success: function(model, response, options) {
                app.handleMessages(response);
            }
        });
        this.$el.fadeOut();
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        this.$input = this.$('.edit-project');
        this.$childInput = this.$('input.add-sub-project');

        return this;
    },
});

$(function() {
    new app.AppView();
});
