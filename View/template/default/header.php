<html>
<head>
    <meta charset="utf-8"/>
    <link rel="shortcut icon" href="/View/template/default/favicon.ico" type="image/x-icon">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="View/template/default/css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="View/template/default/css/bootstrap-theme.min.css">
<?php foreach ($styles as $style):?>
    <link rel="stylesheet" href="<?=$style?>">
<?php endforeach; ?>
</head>
<body>
