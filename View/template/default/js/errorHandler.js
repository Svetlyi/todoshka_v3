var app = app || {};

app.ErrorModel = Backbone.Model.extend({
});

app.ErrorView = Backbone.View.extend({
    tagName: 'div',

    className: 'alert alert-danger alert-dismissible',

    template: _.template($('#error-template').html()),

    initialize: function() {
        this.render()
    },

    events: {
    },

    render: function() {
        $('.row').before(this.template(this.model.toJSON()));
    },
});


app.handleMessages = function(params) {
    var error = new app.ErrorModel({
        'error': params['error'][0]
    });
    var error = new app.ErrorView({model: error});
}


