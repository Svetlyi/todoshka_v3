<a href="#" class="list-group-item">
    <div class="tools">
        <span note-id="<?=$note['id'];?>" class="delete-note glyphicon glyphicon-remove"></span>
    </div>

    <?php for ($i=0;$i<$note['depth'];$i++): ?>
    <div class="delimetr"><span class="glyphicon glyphicon-minus"></span></div>
    <?php endfor;?>

    <h4 class="list-group-item-heading"><?=$note['name'];?></h4>
    <p class="list-group-item-text"><?=$note['description'];?></p>
</a>

