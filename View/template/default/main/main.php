<div class="col-md-9">
    <div class="list-group" id='task_list'>
        <?php
            // include('list.php');
        ?>
    </div>

    <ul class="nav nav-tabs">
        <li role="presentation" class="active">
            <a href="#create_task" role="tab" data-toggle="tab" aria-controls="task" aria-expanded="true">Create task</a>
        </li>
        <li role="presentation">
            <a href="#create_note" role="tab" data-toggle="tab" aria-controls="note" aria-expanded="true">Create note</a>
        </li>
    </ul>

    <div id="tab-content-create" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="create_task" aria-labelledby="task-tab">
            <form class="form-inline" method='POST' id='form_create_task'>
              <div class="form-group">
                <label class="sr-only" for="new_task">New task</label>
                <input type="text" class="form-control" id="new_task" name="name" placeholder="Enter new task"/>
                <input type="datetime-local" class="form-control" name="date"/>
              </div>
            </form>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="create_note" aria-labelledby="note-tab">
            <form class="form" method='POST' id='form_create_note'>
              <div class="form-group">
                <label class="sr-only" for="new_note">New note</label>
                <input type="text" class="form-control" name="name" placeholder="Enter name"/>
                <textarea class="form-control" name="description" placeholder="Enter description" rows="3"></textarea>
                <input type="submit" class="form-control" id="new_note" placeholder="Enter description"/>
              </div>
            </form>
        </div>
    </div>
</div>
<script type="text/template" id="task-template">
    <div class="view">
        <% for(var i=0;i<depth;i++) { %>
        <div class="delimetr"><span class="glyphicon glyphicon-minus"></span></div> 
        <% } %>
        <input type="checkbox" class="task-completed"/>
        <%= name %>

        <div class="tools">
            <span class="add-sub-task glyphicon glyphicon-ok"></span>
            <span class="add-sub-note glyphicon glyphicon-pencil"></span>
            <span class="delete-task glyphicon glyphicon-remove"></span>
        </div>
    </div>

    <input type="text" value="<%= name %>" class="edit-task form-control"/>
    <input type="text" class="add-sub-task form-control"/>
    <div class="add-sub-note">
        <input type="text" class="name form-control" name="name" placeholder="Enter name"/>
        <textarea class="description form-control" name="description" placeholder="Enter description" rows="3"></textarea>
        <button class="submit-sub-note form-control" id="new_sub_note" placeholder="Enter description"/>
    </div>

</script>
<script type="text/template" id="note-template">
    <div class="view">
        <div class="tools">
            <span task-id="<%= id %>" class="delete-task glyphicon glyphicon-remove"></span>
        </div>

        <% for(var i=0;i<depth;i++) { %>
        <div class="delimetr"><span class="glyphicon glyphicon-minus"></span></div> 
        <% } %>
        <h4 class="list-group-item-heading"><%= name %></h4>
        <p class="list-group-item-text"><%= description %></p>
    </div>

    <form class="edit-note form">
        <input type="text" class="edit-note-name form-control" name="name" placeholder="Enter name" value="<%= name %>"/>
        <textarea class="edit-note-description form-control" name="description" placeholder="Enter description" rows="3"><%= description %></textarea>
        <input type="submit" class="edit-note form-control" id="edit_note" placeholder="Enter description"/>
    </form>
</script>
