<a class="list-group-item">
    <?php for ($i=0;$i<$task['depth'];$i++): ?>
    <div class="delimetr"><span class="glyphicon glyphicon-minus"></span></div> 
    <?php endfor;?>
    <input type="checkbox" task-id="<?=$task['id'];?>" class="task-checkbox" <?=($task['completed']==1)?'checked':'';?>/>
    <?=$task['name'];?>

    <div class="tools">
    <?=$task['date'];?>
        <span task-id="<?=$task['id'];?>" class="glyphicon glyphicon-ok"></span>
        <span task-id="<?=$task['id'];?>" class="glyphicon glyphicon-pencil"></span>
        <span task-id="<?=$task['id'];?>" class="delete-task glyphicon glyphicon-remove"></span>
    </div>
</a>
