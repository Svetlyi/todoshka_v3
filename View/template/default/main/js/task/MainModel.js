var app = app || {};

app.project_id = null;

app.getProject = function() {
    return app.project_id;
}
app.setProject = function(id) {
    console.log('current project changed');
    app.project_id = id;

    app.MainTree.stopListening();
    app.MainTree.undelegateEvents();
    app.MainTree = new app.MainBlockView();
    app.Projects.trigger('select_project');
}

app.MainItem = Backbone.Model.extend({
    initialize: function () {
        if (undefined != this.get('completed')) {
            var elem = new app.Task(this.attributes);
            app.Tasks.add(elem);
        } else {
            var elem = new app.Note();
            elem.set(this.attributes);
            app.Notes.add(elem);
        }
    }
}); 

var MainModel = Backbone.Collection.extend({
    url: 'index.php?api&target=main',
    model: app.MainItem,
});

app.MainItems = new MainModel();
