var app = app || {};

app.MainBlockView = Backbone.View.extend({
    el: $('.row'),

    events: {
        'keypress #new_task': 'createTaskOnEnter',
        'click #new_note': 'createNoteOnEnter',
    },

    initialize: function() {
        $('#task_list').empty();
        app.Tasks = new app.TaskList();
        app.Notes = new app.NoteList();
        
        this.$input = this.$('#new_task');
        this.$formAddNote = this.$('#form_create_note');
        this.listenTo(app.Tasks, 'add', this.addTask);
        this.listenTo(app.Notes, 'add', this.addNote);

        app.MainItems.fetch({
            data: {
                project: app.getProject()
            },
            success: function() {
                app.MainItems.add(app.MainItems.attributes);
                console.log('Items received:');
                console.log(app.MainItems.toJSON());
            },
            error: function() {
                console.log('fetch failed');
            },
        });
    },

    lalala: function() {
        console.log('lalallalal');
    },

    addTask: function(task) {
        var view = new app.TaskView({model: task});
        $('#task_list').append(view.render().el);

        console.log('adding task');
        console.log(task);
        if(task.get('children_task').length>0) {
            _.each(task.get('children_task').models, function(children) {
                app.Tasks.add(children);
            });
        }
        if(task.get('children_note').length>0) {
            _.each(task.get('children_note').models, function(children) {
                app.Notes.add(children);
            });
        }
    },

    addNote: function(note) {
        console.log('Rendering note');
        var view = new app.NoteView({model: note});
        $('#task_list').append(view.render().el);
    },

    newAttributes: function() {
        return {
            name: this.$input.val().trim(),
            depth: 0,
        }
    },

    createNoteOnEnter: function() {
        event.preventDefault();
        var params = {};
        var data = this.$formAddNote.serializeArray();
        _.each(data, function(param) {
            params[param['name']] = param['value'];
        })
        this.$formAddNote.find("input[type=text], textarea").val("");
        params['project_id'] = app.getProject();
        data = params;

        app.Notes.create(data, {
            success: function(note) {
            },
            wait: true,
        });
        console.log(app.Notes.toJSON());
        this.$input.val('');
    },

    createTaskOnEnter: function(event) {
        if(event.which !== ENTER_KEY) {
            return;
        }
        event.preventDefault();

        var params = this.newAttributes();
        params['project_id'] = app.getProject();
        app.Tasks.create(params, {
            success: function(task) {
            },
            wait: true,
        });
        console.log(app.Tasks.toJSON());
        this.$input.val('');
    }
});

$(function() {
    app.MainTree = new app.MainBlockView();
});
