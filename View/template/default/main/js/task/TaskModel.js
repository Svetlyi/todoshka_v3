var app = app || {};
var ENTER_KEY = 13;

app.Task = Backbone.Model.extend({
    url: 'index.php?api&target=task',
    defaults: {},

    initialize: function() {
        if(Array.isArray(this.get('children'))) {
            var noteArray = [];
            var taskArray = [];
            _.forEach(this.get('children'),function(child){
                if ('completed' in child) {
                    taskArray[taskArray.length] = child;
                } else {
                    noteArray[noteArray.length] = child;
                }
            });
            this.set({children_task: new app.TaskChildren(taskArray)});
            this.set({children_note: new app.NoteChildren(noteArray)});
        } else {
            this.set({children_task: new app.TaskChildren()});
            this.set({children_note: new app.NoteChildren()});
        }
    },

    sync: function (method, model, options) {
        if(method === 'delete') {
            options = options || {};
            options.contentType = 'application/json';
            options.data = JSON.stringify(this.toJSON());
        }

        return Backbone.sync.call(this, method, model, options);
    },

    addChildrenNote: function(note) {
        console.log('note');
    }
});

app.TaskChildren = Backbone.Collection.extend({
    url: 'index.php?api&target=main',
    model: app.Task,
});

app.NoteChildren = Backbone.Collection.extend({
    url: 'index.php?api&target=main',
    model: app.Note,
});

app.TaskList = Backbone.Collection.extend({
    url: 'index.php?api&target=main',
    model: app.Task,
});
