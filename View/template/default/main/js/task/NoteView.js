var app = app || {};

app.NoteView = Backbone.View.extend({
    tagName: 'a',

    className: 'list-group-item',

    template: _.template($('#note-template').html()),

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },

    events: {
        'click .delete-task': 'delete',

        'dblclick': 'edit',
        'click input.edit-note': 'close',
    },

    edit: function() {
        this.$el.addClass('edit');
    },

    close: function(e) {
        e.preventDefault();
        this.model.save({
            name: this.$el.find('.edit-note-name').val(),
            description: this.$el.find('.edit-note-description').val(),
        });
        this.$el.removeClass("edit");
    },

    delete: function() {
        this.model.destroy({
            success: function(model, response, options) {
                app.handleMessages(response);
            }
        });
        this.$el.fadeOut();
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        this.$input = this.$('.edit-task');
        this.$childInput = this.$('input.add-sub-task');

        return this;
    },
});

