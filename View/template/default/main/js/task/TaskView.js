var app = app || {};

app.TaskView = Backbone.View.extend({
    tagName: 'a',

    className: 'list-group-item',

    template: _.template($('#task-template').html()),

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
        this.listenTo(this.model.get('children_task'), 'add', this.addChildrenTask);
        this.listenTo(this.model.get('children_note'), 'add', this.addChildrenNote);
    },

    events: {
        'click .delete-task': 'delete',

        'click .add-sub-task': 'showFormAddSubTask',
        'blur .add-sub-task': 'closeFormAddSubTask',
        'keypress input.add-sub-task': 'saveSubTaskOnEnter',

        'click span.add-sub-note': 'showFormAddSubNote',
        'click div.add-sub-note .submit-sub-note': 'closeFormAddSubNote',

        'dblclick': 'edit',
        'keypress .edit-task': 'updateOnEnter',
        'blur .edit-task': 'close',
    },

    edit: function() {
        this.$el.addClass('edit');
    },
    //SubNotes
    showFormAddSubNote: function() {
        this.$el.addClass('form-add-sub-note');
    },

    closeFormAddSubNote: function(e) {
        console.log('closing form and saving');
        var name = this.$inputSubNoteName.val();
        var description = this.$inputSubNoteDescription.val();
        if (name.length > 0 && description.length > 0) {
            this.$inputSubNoteName.val('');
            this.$inputSubNoteDescription.val('');

            this.model.get('children_note').create({
                name: name,
                project_id: app.getProject(),
                description: description,
                task_id: this.model.get('id')
            }, {
                success: function (task) {
                },
                wait: true
            });
            this.$el.removeClass('form-add-sub-note');
        }
    },
    addChildrenNote: function (note) {
        var view = new app.NoteView({model: note});
        this.$el.after(view.render().el);
    },

    //SubTasks
    showFormAddSubTask: function() {
        this.$el.addClass('form-add-sub-task');
    },

    closeFormAddSubTask: function() {
        var value = this.$childInput.val();
        if (value.length > 0) {
            this.$childInput.val('');

            this.model.get('children_task').create({
                name: value,
                project_id: app.getProject(),
                task_id: this.model.get('id')
            }, {
                success: function (task) {
                },
                wait: true
            });
            this.$el.removeClass('form-add-sub-task');
        }
    },

    saveSubTaskOnEnter: function (e) {
        if(e.keyCode == 13) {
            this.closeFormAddSubTask();
        }
    },

    addChildrenTask: function (task) {
        console.log('adding child note');
        task.set({children_task: new app.TaskChildren(task.get('children_task'))});
        var view = new app.TaskView({model: task});
        this.$el.after(view.render().el);
    },

    close: function() {
        var value = this.$input.val();
        this.model.save({name: value});
        this.$el.removeClass("edit");
    },

    updateOnEnter: function(e) {
        if(e.keyCode == 13) {
            this.close()
        }      
    },

    delete: function() {
        this.model.destroy({
            success: function(model, response, options) {
                app.handleMessages(response);
            }
        });
        this.$el.fadeOut();
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        this.$input = this.$('.edit-task');
        this.$childInput = this.$('input.add-sub-task');
        this.$inputSubNoteName = this.$('div.add-sub-note .name');
        this.$inputSubNoteDescription = this.$('div.add-sub-note .description');
        this.$inputSubNoteSubmit = this.$('div.add-sub-note .submit-sub-note');

        return this;
    },
});
