var app = app || {};
var ENTER_KEY = 13;

app.Note = Backbone.Model.extend({
    url: 'index.php?api&target=note',
    defaults: {},

    initialize: function() {
        console.log('init note');
    },

    sync: function (method, model, options) {
        if(method === 'delete') {
            options = options || {};
            options.contentType = 'application/json';
            options.data = JSON.stringify(this.toJSON());
        }

        return Backbone.sync.call(this, method, model, options);
    }
});

app.NoteChildren = Backbone.Collection.extend({
    url: 'index.php?api&target=note',
    model: app.Note,
});

app.NoteList = Backbone.Collection.extend({
    url: 'index.php?api&target=note',
    model: app.Note,
});
