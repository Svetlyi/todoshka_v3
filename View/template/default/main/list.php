<?php 
foreach ($records as $item) {
    if (array_key_exists('completed', $item)) {
        $task = $item;
        include('task.php');
    } else {
        $note = $item;
        include('note.php');
    }
    if (isset($item['children'])) {
        $parent_records = $records;
        $records = $item['children'];
        include('list.php');
        $records = $parent_records;
    }
}
?>
