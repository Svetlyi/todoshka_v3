<?php
function err_handler($errno, $errmsg, $filename, $linenum) {
    if (!in_array($errno, Array(E_NOTICE, E_STRICT, E_WARNING))) {
        $date = date('Y-m-d H:i:s (T)');
        $f = fopen('./log/errors.php', 'a');
        if (!empty($f)) {
            $err  = "\r\n";
            $err .= "  $date\r\n";
            $err .= "  $errno\r\n";
            $err .= "  $errmsg\r\n";
            $err .= "  $filename\r\n";
            $err .= "  $linenum\r\n";
            $err .= "\r\n";
            fwrite($f, $err);
            fclose($f);
        }
    }
}
set_error_handler('err_handler');



session_start();
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once('./Controller/Core/Autoloader.php');

$config = require('config.php');
App::getInstance()->config = $config;
Router::getInstance()->route();
//PAGES
//$page = (isset($_GET['page']))?$_GET['page']:'default';
//switch ($page) {
//    case 'main':
//        $user = new User(array(
//            'id'=>$_SESSION['id']
//        ));
//        $projects = $user->getProjects();
//        require('./View/template/default/main.php');
//        break;
//    case 'login':
//        require('./View/template/default/login.php');
//        break;
//    case 'registration':
//        require('./View/template/default/registration.php');
//        break;
//    case 'task_list':
//        $user = new User(array(
//            'id'=>$_SESSION['id']
//        ));
//        //$tasks = $user->getTasks();
//        require('./View/template/default/task_list.php');
//        break;
//    case 'note_list':
//        $user = new User(array(
//            'id'=>$_SESSION['id']
//        ));
//        //$tasks = $user->getNotes();
//        require('./View/template/default/note_list.php');
//        break;
//}
