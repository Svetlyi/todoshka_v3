<?php
class ProjectController extends Controller {
    public function accessRules() 
    {
        return array(
            array('allow',
                'actions'=>array('create', 'delete', 'update'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            ),
            array('allow',
                'actions'=>array('index'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            )
        );
    }

    public function createAction() {
        $project = new Project();
        $options = $_POST;
        if (Router::getInstance()->isApi('api')) {
            $options = json_decode(file_get_contents('php://input'), true);
        }
        $options['user_id'] = Router::getInstance()->user->getId();
        //if (Router::getInstance()->getPost('project_id')) {
        if (isset($options['project_id'])) {
            $parentProject = Project::find("id=:id AND user_id=:user_id", array(
        //        ':id'=>Router::getInstance()->getPost('project_id'),
                ':id'=>$options['project_id'],
                ':user_id'=>Router::getInstance()->user->getId()
            ));
            if ($parentProject) {
                $options['depth'] = $parentProject['depth'] + 1;
            } else {
                $options['depth'] = 0;
            }
        }
        $project->set($options);
        $project->save();
        $user = new User(array(
            'id'=>$_SESSION['id']
        ));

        if (Router::getInstance()->isApi('api')) {
            header('Content-Type: application/json');
            echo json_encode(array(
                'id'=>$project->getId(),
                'depth'=>$project->getDepth()
            ));
        }
    }

    public function updateAction() {
        if (Router::getInstance()->isApi('api')) {
            $options = json_decode(file_get_contents('php://input'), true);
            $project = new Project();
            //TODO check for 'depth'
            $project->set($options);
            $project->update("user_id=:user_id AND id=:id", array(
                ':user_id'=>Router::getInstance()->user->getId(),
                ':id'=>$options['id']
            ));
        }
    }

    public function deleteAction() {
        $id = Router::getInstance()->getPost('id');
        if (Router::getInstance()->isApi('api')) {
            $options = json_decode(file_get_contents('php://input'), true);
            $id = $options['id'];
        }
        $subProjects = Project::findAll("user_id=:user_id AND project_id=:project_id", array(
            ':user_id'=>Router::getInstance()->user->getId(),
            ':project_id'=>$id
        ));
        if($subProjects) {
            App::getInstance()->addErrorMessage("There are some child elements. You should delete them!");
        } else {
            Project::delete("user_id=:user_id AND id=:id", array(
                ':user_id'=>Router::getInstance()->user->getId(),
                ':id'=>$id
            ));
        }
    }

    public function indexAction() {
        $projects = Project::findAll("user_id=:user_id", array(
            ':user_id'=>Router::getInstance()->user->getId()
        ));

        $mainController = new MainController();
        $projects = $mainController->getTree($projects, array(
            'parent_key'=>'project_id',
            'key'=>'id'
        ));
        if (Router::getInstance()->isApi()) {
            header('Content-Type: application/json');
            echo json_encode($projects);
            return;
        } 

        $projects = array();

        $this->registerScriptFile('js/script.js');
        $this->registerScriptFile('js/backbone.js');
        $this->registerCssFile('css/style.css');
        $this->render('project_list', array(
            'projects'=>$projects
        ));
    }

}
