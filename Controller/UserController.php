<?php
class UserController extends Controller {
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index', 'login', 'registration','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','logout'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

    public function logoutAction() {
        unset($_SESSION['id']);
        header( 'Location: /', true, 303);
    }

    public function loginAction() {
        if (Router::getInstance()->isPost()) {
            $user = new User();
            $user->set($_POST);

            $userData = User::find("name=:name", array(
                ':name'=>$user->getName('name')
            ));

            if ($userData) {
                $timeLeft = time() - $userData['last_encorrect_login_attempt'];
                if ($timeLeft > 20) {
                    if ($userData['password'] == $user->getPassword()) {
                        $user = new User();
                        $user->set($userData);
                        Router::getInstance()->user = $user;
                        $_SESSION['id'] = $user->getId();
                        App::getInstance()->addSuccessMessage("Welcome");
                    } else {
                        App::getInstance()->addErrorMessage("Incorrect password. You can enter in 20 seconds");
                        unset($user);
                        $user = new User();
                        $params = array(
                            'last_encorrect_login_attempt' => time()
                        );
                        $user->set($params);
                        $user->update("id=:id", array(
                            ':id'=>$userData['id']
                        ));
                    }
                } else {
                    $timeLeft = 20 - $timeLeft;
                    App::getInstance()->addErrorMessage("You can enter in $timeLeft seconds");
                }
            } else {
                App::getInstance()->addErrorMessage("Incorrect login");
            }
        }
        $this->render('login');
        $this->show();
    }

    public function registrationAction() {
        if (Router::getInstance()->isPost()) {
            if (strlen($_POST['name'])>3 && strlen($_POST['password'])>3 && strlen($_POST['email'])>3) {
                $sameDataUser = User::find("name=:name OR email=:email", array(
                    ':name'=>$_POST['name'],
                    ':email'=>$_POST['email']
                ));
                if (empty($sameDataUser)) {
                    $user = new User();
                    $options = $_POST;
                    $role = Role::find('name=:name', array(
                        ':name'=>'user'
                    ));
                    $options['role_id'] = $role['id'];
                    $user->set($options);
                    $user->save();
                    App::getInstance()->addSuccessMessage("Welcome. You can log in now!");
                } else {
                    App::getInstance()->addErrorMessage("There is user with the same name or email");
                }
            } else {
                App::getInstance()->addErrorMessage("Incorrect input data");
            }
        }
        $this->render('registration');
        $this->show();
    }
}
