<?php
class App {
    private static $instance;
    public $styles = array();
    public $scripts = array();
    public $successMsg = array();
    public $errorMsg = array();
    public $modules;
    public $config;

    private function __construct() {}

    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function addSuccessMessage($msg) {
        $this->successMsg[] = $msg;
    }

    public function addErrorMessage($msg) {
        $this->errorMsg[] = $msg;
    }
}
