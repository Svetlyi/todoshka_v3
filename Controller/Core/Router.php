<?php
class Router {
    public $params = array();

    private static $instance;

    public $user=null;

    private function __construct() {
        $this->content = '';
        $this->params = array_merge($_GET, $_POST);
        if (isset($_SESSION['id'])) {
            $user = User::find("id=:id", array(
                ':id'=>$_SESSION['id']
            ));
            if ($user) {
                $this->user = new User();
                $this->user->set($user);
            }
        }
    }

    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function checkRules($controller, $action) {
        $result = false;
        $rules = $controller->accessRules();
        foreach($rules as $access=>$rule) {
            $access = (in_array('allow', $rule))?true:false;
            if (in_array($action, $rule['actions'])) {
                if (isset($rule['groups'])) {
                    foreach ($rule['groups'] as $group) {
                        switch ($group) {
                            case 'admin':
                                if (isset($this->user) && $this->user->isAdmin())
                                    $result = true && $access;
                                    break;
                            case '@':
                                if (isset($this->user))
                                    $result = true && $access;
                                    break;
                            case '*':
                                    $result = true && $access;
                                    break;
                        }
                    }
                } else {
                    $result = true && $access;
                }
            }
        }

        return $result;
    }

    public function getPost($param) {
        return (isset($_POST[$param]))?$_POST[$param]:null;
    }

    public function getGet($param) {
        return (isset($_GET[$param]))?$_GET[$param]:null;
    }

    public function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest';
    }

    public function isPost() {
        return ($_SERVER['REQUEST_METHOD'] == 'POST');
    }

    public function isApi() {
        return isset($_POST['api'])||isset($_GET['api']);
    }

    public function getData() {
        if ($this->isApi()) {
            $options = json_decode(file_get_contents('php://input'), true);
        }

        return $options;
    }

    public function routeApi() {
        $action = 'index';
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST': 
                $action = 'create';
                break;
            case 'GET': 
                $action = 'index';
                break;
            case 'PUT': 
                $action = 'update';
                break;
            case 'DELETE': 
                $action = 'delete';
                break;
        }
        $target = $_GET['target'];
        $controller = ucfirst($target).'Controller';
        if (class_exists($controller)) {
            $controller = new $controller();
            if ($this->checkRules($controller, $action)) {
                $action = $action.'Action';
                if (!method_exists($controller, $action)) {
                    $action = 'indexAction';
                }
                $controller->{$action}();
            } else {
                unset($_SESSION['id']);
                App::getInstance()->addErrorMessage("You are not allowed. Rules issue");
            }
        }
        $this->showMessages(get_class($controller), $action);
    }

    private function showMessages($controller, $action) {
        $error = App::getInstance()->errorMsg;
        $success = App::getInstance()->successMsg;
        if ($error || $success) {
            header('Content-Type: application/json');
            echo json_encode(array(
                'error'=>$error,
                'success'=>$success,
                'class'=>$controller,
                'action'=>$action
            ));
        }
    }

    public function route() {
        if ($this->isApi()) {
            $this->routeApi();
        } else {
            if (!isset($this->params['target'])) {
                $this->params['target'] = 'main';
            }
            $controller = ucfirst($this->params['target']).'Controller';
            $action = (isset($this->params['action']))?$this->params['action']:'index';
            if (class_exists($controller)) {
                $controller = new $controller();
                if ($this->checkRules($controller, $action)) {
                    $action = $action.'Action';
                    if (!method_exists($controller, $action)) {
                        $action = 'indexAction';
                    }
                    $controller->{$action}();
                } else {
                    unset($_SESSION['id']);
                    App::getInstance()->addErrorMessage("You are not allowed. Please login");
                    $userController = new UserController();
                    $userController->loginAction();
                }
            }
        }
    }
}
