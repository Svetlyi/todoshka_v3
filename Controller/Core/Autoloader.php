<?php
function modelAutoloader($class) {
    $path = './Model/'.$class.'.php';
    if (file_exists($path)) {
        include $path;
    }
}
function controllerAutoloader($class) {
    $path = './Controller/'.$class.'.php';
    if (file_exists($path)) {
        include $path;
    }
}
function coreAutoloader($class) {
    $path = './Controller/Core/'.$class.'.php';
    if (file_exists($path)) {
        include $path;
    }
}
spl_autoload_register('coreAutoloader');
spl_autoload_register('controllerAutoloader');
spl_autoload_register('modelAutoloader');
