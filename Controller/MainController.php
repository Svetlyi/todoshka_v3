<?php
class MainController extends Controller {
    public function accessRules() 
    {
        return array(
            array('allow',
                'actions'=>array('create', 'delete', 'update'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            ),
            array('allow',
                'actions'=>array('index'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            )
        );
    }

    public function indexAction() {
        if (!Router::getInstance()->isApi()) {
            $projectController = new ProjectController();
            $projectController->indexAction();
        }

        $project_id = Router::getInstance()->getGet('project');

        $query = "user_id=:user_id";
        $params = array(
            ':user_id'=>Router::getInstance()->user->getId()
        );

        if (!is_numeric($project_id)) {
            $query.= " AND project_id IS NULL";
        } else {
            $query.= " AND project_id=:project_id";
            $params[":project_id"] = $project_id;
        }
        $tasks = Task::findAll($query, $params);
        $notes = Note::findAll($query, $params);
        $records = array_merge($tasks, $notes);

        $records = $this->getTree($records, array(
            'key'=>'id',
            'parent_key'=>'task_id'
        ));

        if (Router::getInstance()->isApi()) {
            header('Content-Type: application/json');
            echo json_encode($records);
            return;
        } 

        $this->registerCssFile('css/style.css');
        $this->registerScriptFile('js/task/NoteModel.js');
        $this->registerScriptFile('js/task/NoteView.js');
        $this->registerScriptFile('js/task/TaskModel.js');
        $this->registerScriptFile('js/task/TaskView.js');
        $this->registerScriptFile('js/task/MainView.js');
        $this->registerScriptFile('js/task/MainModel.js');
//        $this->registerScriptFile('js/script.js');
        $this->render('main', array(
            'records'=>$records
        ));
        $this->show();
    }

    /*
     * options
     *
     * key: identifier
     * parent_key: parent identifier
     * */
    public function getTree($input, $options=array()) {
        $all = array();
        $dangling = array();
        $output = array();

        foreach ($input as $entry) {
            $entry['children'] = array();
            $id = $entry[$options['key']]; //replace id with 'orig_id'

            if ($entry[$options['parent_key']] == NULL) {
                $all[$id] = $entry;
                $output[] =& $all[$id];
            } else {
                $dangling[$id] = $entry;
            }
        }

        //'dangling' nodes
        while (count($dangling) > 0) {
            foreach ($dangling as $entry) {
                $id = $entry[$options['key']];
                $pid = $entry[$options['parent_key']];

                if(isset($all[$pid])) {
                    $all[$id] = $entry;
                    $all[$pid]['children'][] =& $all[$id];
                    //$all[$pid]['children'][] = $entry;
                    unset($dangling[$entry[$options['key']]]);
                }
            }
        }

        return $output;
    }

//    public function updateAction() {
//        die(var_dump(Router::getInstance()->getData()));
//        $taskController = new TaskController();
//        $taskController->updateAction();
//    }
//
//    public function deleteAction() {
//        $taskController = new TaskController();
//        $taskController->deleteAction();
//    }
}
