<?php
class TaskController extends Controller {
    public function accessRules() 
    {
        return array(
            array('allow',
                'actions'=>array('create', 'delete', 'update'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            ),
            array('allow',
                'actions'=>array('index'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            )
        );
    }

    public function createAction() {
        $params = Router::getInstance()->getData();
        $params['user_id'] = Router::getInstance()->user->getId();
        $params['date'] = (Router::getInstance()->getPost('date'))?:date("Y-m-d H:i:s");

        if (isset($params['task_id'])) {
            $parentTask = Task::find('id=:task_id AND user_id=:user_id', array(
                ':task_id'=>$params['task_id'],
                ':user_id'=>Router::getInstance()->user->getId()
            ));
            $params['depth'] = $parentTask['depth'] + 1;
        }
        $task = new Task();
        $task->set($params);
        $task->save();

        if (Router::getInstance()->isAjax()) {
            $result = array(
                'success'=>1,
                'id'=>$task->getId(),
                'depth'=>$task->getDepth()
            );
            echo json_encode($result);
        }
    }

    public function updateAction() {
        $params = Router::getInstance()->getData();
        if (isset($params['completed'])) {
            $params['completed'] = ($params['completed'] == 1)?1:0;
        }
        $task = new Task();
        $task->set($params);
        $task->update("(id=:id OR task_id=:task_id) AND user_id=:user_id", array(
            ':id'=>$params['id'],
            ':task_id'=>$params['id'],
            ':user_id'=>Router::getInstance()->user->getId()
        ));
    }

    public function deleteAction() {
        $options = Router::getInstance()->getData();
        $childTasks = Task::findAll("user_id=:user_id AND task_id=:task_id", array(
            ':user_id'=>Router::getInstance()->user->getId(),
            ':task_id'=>$options['id']
        ));
        $childNotes = Note::findAll("user_id=:user_id AND task_id=:task_id", array(
            ':user_id'=>Router::getInstance()->user->getId(),
            ':task_id'=>$options['id']
        ));
        if ($childTasks || $childNotes) {
            App::getInstance()->addErrorMessage("There are some child elements. You should delete them!");
        } else {
            Task::delete("id=:id AND user_id=:user_id", array(
                ':id'=>$options['id'],
                ':user_id'=>Router::getInstance()->user->getId(),
            ));
        }
    }

    public function indexAction() {
        $project_id = Router::getInstance()->getPost('project_id');

        $query = "user_id=:user_id";
        $params = array(
            ':user_id'=>Router::getInstance()->user->getId()
        );

        if (NULL === $project_id) {
            $query.= " AND project_id IS NULL";
        } else {
            $query.= " AND project_id=:project_id";
            $params[":project_id"] = $project_id;
        }
        $tasks = Task::findAll($query, $params);
        if (Router::getInstance()->isApi()) {
            header('Content-Type: application/json');
            echo json_encode($tasks);
        } else {
            $this->registerScriptFile('js/script.js');
            $this->registerCssFile('css/style.css');
            $this->render('task_list', array(
                'tasks'=>$tasks
            ));
            $this->show();
        }
    }
}
