<?php
class Controller {
    function __construct() {}

    public function render($template = null, $params = array()) {
        $class = strtolower(str_replace("Controller", "", get_class($this)));
        $template = $class.'/'.$template;
        App::getInstance()->modules[] = array(
            'template'=>$template,
            'params'=>$params
        );
    }

    public function show() {
        $styles = App::getInstance()->styles;
        $scripts = App::getInstance()->scripts;
        $errors = App::getInstance()->errorMsg;
        $successes = App::getInstance()->successMsg;
        require('./View/template/default/header.php');
        require('./View/template/default/top_menu.php');

        foreach (App::getInstance()->modules as $module) { 
            $this->renderPartial($module['template'], $module['params']);
        }
        //TODO make grid of modules
        //можно засовывать в какой-нибудь App::addContent($template);
        //потом это все выводить в Router
        require('./View/template/default/footer.php');
    }

    public function renderPartial($template, $params = array()) {
        foreach ($params as $key=>$val) {
            ${$key} = $val;
        }

        require('./View/template/default/'.$template.'.php');
    }

    public function registerCssFile($file) {
        $class = strtolower(str_replace("Controller", "", get_class($this)));
        App::getInstance()->styles[] = './View/template/default/'.$class.'/'.$file;
    }

    public function registerScriptFile($file) {
        $class = strtolower(str_replace("Controller", "", get_class($this)));
        App::getInstance()->scripts[] = './View/template/default/'.$class.'/'.$file;
    }

}
