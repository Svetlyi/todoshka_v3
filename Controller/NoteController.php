<?php
class NoteController extends Controller {
    public function accessRules() 
    {
        return array(
            array('allow',
                'actions'=>array('create', 'delete', 'update'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            ),
            array('allow',
                'actions'=>array('index'),
                'groups'=>array('@'),
                'mode'=>array('POST', 'AJAX')
            )
        );
    }

    public function createAction() {
        $params = Router::getInstance()->getData();
        $params['user_id'] = Router::getInstance()->user->getId();
        $params['project_id'] = $params['project_id'];
        if (isset($params['task_id'])) {
            $parentTask = Task::find('id=:task_id AND user_id=:user_id', array(
                ':task_id'=>$params['task_id'],
                ':user_id'=>Router::getInstance()->user->getId()
            ));
            $params['depth'] = $parentTask['depth'] + 1;
        }

        $note = new Note();
        $note->set($params);
        $note->save();

        if (Router::getInstance()->isApi('api')) {
            header('Content-Type: application/json');
            echo json_encode(array(
                'id'=>$note->getId(),
                'depth'=>$note->getDepth()
            ));
        }
    }

    public function updateAction() {
        $params = Router::getInstance()->getData();
        $note = new Note();
        $note->set($params);
        $note->update("id=:id AND user_id=:user_id", array(
            ':id'=>$params['id'],
            ':user_id'=>Router::getInstance()->user->getId()
        ));
    }

    public function deleteAction() {
        $options = Router::getInstance()->getData();
        Note::delete("id=:id AND user_id=:user_id", array(
            ':id'=>$options['id'],
            ':user_id'=>Router::getInstance()->user->getId(),
        ));
    }

    public function indexAction() {
        $notes = Router::getInstance()->user->getNotes();

        $this->registerScriptFile('js/script.js');
        $this->registerCssFile('css/style.css');
        $this->render('note_list', array(
            'notes'=>$notes
        ));
    }
}
