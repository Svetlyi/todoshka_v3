<?php
class Module extends Model {
    private $id;
    private $name;
    private $description;
    private $folder;
    private $image;
    private $row;
    private $position;
    private $enabled;

    function __construct () {
        parent::__construct();    
    }
    //GETTERS
    public function getId() {
        return $this->id;
    }

    public function save () {
        $project = $this->dbh->prepare("INSERT INTO projects (name, color) VALUES (:name, :color)");
        $project->execute(array(
            ':name'=>$this->name,
            ':color'=>$this->color
        ));
        $this->id = $this->dbh->lastInsertId();
    }

    public function delete() {
        $user_has_project = $this->dbh->prepare("SELECT * FROM users_has_projects WHERE user_id=:user_id AND project_id=:project_id");
        $user_has_project->execute(array(
            ':user_id'=>Router::getInstance()->user->getId(),
            'project_id'=>$this->id
        ));
        $user_has_project = $user_has_project->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($user_has_project)) {
            $user_has_project = $this->dbh->prepare("DELETE FROM users_has_projects WHERE user_id=:user_id AND project_id=:project_id");
            $user_has_project->execute(array(
                ':user_id'=>Router::getInstance()->user->getId(),
                'project_id'=>$this->id
            ));
            $project = $this->dbh->prepare("DELETE FROM projects WHERE id=:id");
            $project->execute(array(
                ':id'=>$this->id
            ));
        }
    }
}
