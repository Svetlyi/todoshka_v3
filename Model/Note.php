<?php
class Note extends Model {
    protected $id;
    protected $name;
    protected $description;
    protected $color;
    protected $depth;
    protected $project_id;
    protected $task_id;
    protected $user_id;

    public static function getTableName() {
        return 'notes';
    }

    public static function getTableColumns() {
        return array(
            'id',
            'name',
            'description',
            'color',
            'depth',
            'project_id',
            'task_id',
            'user_id'
        );
    }

    private function genId() {
         $maxId = $this->dbh->prepare("
            SELECT max(id) AS id FROM (
                (SELECT max(id) AS id from tasks) 
                UNION 
                (SELECT max(id) AS id FROM notes)
            ) AS T
        ");
        $maxId->execute();
        $maxId = $maxId->fetch(PDO::FETCH_NUM);

        if (NULL != $maxId) {
            $maxId = $maxId[0];
            $maxId++;
            $this->id = $maxId;
        } else {
            $this->id = 1;
        }
    }

    //GETTERS
    public function getId() {
        return $this->id;
    }

    public function getDepth() {
        return $this->depth;
    }

    public function save() {
        $this->genId();
        parent::save();
    }
}
