<?php
class Project extends Model {
    protected $id;
    protected $name;
    protected $color;
    protected $depth;
    protected $project_id;
    protected $user_id;

    function __construct () {
        parent::__construct();    
    }

    public static function getTableName() {
        return 'projects';
    }

    public static function getTableColumns() {
        return array(
            'id',
            'name',
            'color',
            'depth',
            'project_id',
            'user_id'
        );
    }

    //GETTERS
    public function getId() {
        return $this->id;
    }

    public function getDepth() {
        return $this->depth;
    }
}
