<?php
class Model {
    protected $dbh;

    function __construct() {
        try {
            $user = App::getInstance()->config['database']['user'];
            $pass = App::getInstance()->config['database']['password'];
            $dbname = App::getInstance()->config['database']['dbname'];
            $this->dbh = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getTableName() {
        return strtolower(get_called_class());
    }

    public function set ($options) {
        foreach ($options as $option=>$value) {
            if (property_exists($this, $option)) {
                $this->{$option} = $value;
            }
        }
    }

    public function getDBH() {
        return $this->dbh;
    }

    public static function findAll ($condition = null, $options = array(), $mode=PDO::FETCH_ASSOC) {
        $class = get_called_class();
        $table = $class::getTableName();
        $query = "SELECT * FROM ".$table;
        $query.= (NULL != $condition)?" WHERE ".$condition:"";
        $model = new Model();
        $sth = $model->getDBH()->prepare($query);
        $sth->execute($options);

        return $sth->fetchAll($mode);
    }

    public static function find ($condition = null, $options = array(), $mode=PDO::FETCH_ASSOC) {
        $class = get_called_class();
        $table = $class::getTableName();
        $query = "SELECT * FROM ".$table;
        $query.= (NULL != $condition)?" WHERE ".$condition:"";
        $model = new Model();
        $sth = $model->getDBH()->prepare($query);
        $sth->execute($options);

        return $sth->fetch($mode);
    }

    public function save () {
        $class = get_called_class();
        //Preparing value row and column row
        $columns = array();
        $options = array();
        foreach ($class::getTableColumns() as $column) {
            if (isset($this->{$column})) {
                $columns[] = $column;
                $key = ':'.$column;
                $options[$key] = $this->{$column};
            }
        }
        $valueRow = '';
        $columnRow = '';
        foreach ($columns as $column) {
            $valueRow.=':'.$column.', ';
            $columnRow.=$column.', ';
        }
        $valueRow = substr($valueRow, 0, -2);
        $columnRow = substr($columnRow, 0, -2);

        //Preparin INSERT query
        $queryInsert = "INSERT INTO ".$class::getTableName()." (".$columnRow.") VALUES (".$valueRow.")";

        $entity = $this->dbh->prepare($queryInsert);
        $entity->execute($options);
        $this->id = $this->dbh->lastInsertId();
    }

    public function update($condition, $options) {
        $class = get_called_class();
        //Preparing value row and column row
        $columns = array();
        foreach ($class::getTableColumns() as $column) {
            if (isset($this->{$column}) && $column!= 'id' && $column!='user_id') {
                $columns[] = $column;
                $key = ':new_'.$column;
                $options[$key] = $this->{$column};
            }
        }
        $setRow = '';
        foreach ($columns as $column) {
            $setRow.=$column.'=';
            $setRow.=':new_'.$column.', ';
        }
        $setRow = substr($setRow, 0, -2);

        //Preparin INSERT query
        $queryUpdate = "UPDATE ".$class::getTableName()." SET ".$setRow;
        $queryUpdate.= (NULL != $condition)?" WHERE ".$condition:"";

        $entity = $this->dbh->prepare($queryUpdate);
        $entity->execute($options);
        $this->id = $this->dbh->lastInsertId();
    }

    public static function delete ($condition = null, $options = array()) {
        $class = get_called_class();
        $table = $class::getTableName();
        $query = "DELETE FROM ".$table;
        $query.= (NULL != $condition)?" WHERE ".$condition:"";
        $model = new Model();
        $sth = $model->getDBH()->prepare($query);
        $result = $sth->execute($options);

        return $result;
    }

}
