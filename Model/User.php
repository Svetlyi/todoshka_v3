<?php
class User extends Model {
    protected $id;
    protected $name;
    protected $email;
    protected $password;
    protected $role_id;
    protected $last_encorrect_login_attempt;

    function __construct() {
        parent::__construct();    
    }

    public static function getTableName () {
        return 'users';
    }

    public static function getTableColumns () {
        return array(
            'id',
            'name',
            'email',
            'password',
            'role_id',
            'last_encorrect_login_attempt'
        );
    }
    //SETTERS
    public function set($options) {
        parent::set($options);
        if ($options['password']) {
            $salt = "ah2*ah2j)_";
            $this->password = md5($salt.md5($options['password']));
        }
    }

    //GETTERS 
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getPassword() {
        return $this->password;
    }

    public function isAdmin() {
        return true;
    }

    public function login() {
        $user = User::findAll("name=:name AND password=:password", array (
            ':name'=>$this->name,
            ':password'=>$this->password
        ));

        if($user) {
            $_SESSION['id'] = $user[0]['id'];
            echo 'success';
        } else {
            echo 'proval';
        }
    }

    public function addProject ($project) {
        $sth = $this->dbh->prepare("INSERT INTO users_has_projects (user_id, project_id) VALUES (:user_id, :project_id)");
        $sth->execute(array(
            ':user_id'=>$this->id,
            'project_id'=>$project->getId()
        ));
    }

    public function getProjects () {
        $projects = $this->dbh->prepare("SELECT * FROM projects WHERE id IN (SELECT project_id FROM users_has_projects WHERE user_id=:user_id)"); 
        $projects->execute(array(
            ':user_id'=>$this->id
        ));
        $projects = $projects->fetchAll(PDO::FETCH_ASSOC);

        return $projects;
    }
}
