<?php
class Role extends Model {
    protected $id;
    protected $name;

    public static function getTableName() {
        return 'roles';
    }

    public static function getTableColumns() {
        return array(
            'id',
            'name'
        );
    }

}
