<?php
class Task extends Model {
    protected $id;
    protected $name;
    protected $date;
    protected $completed;
    protected $depth;
    protected $project_id;
    protected $task_id;
    protected $user_id;

    function __construct () {
        parent::__construct();    
        $this->completed = 0;
    }

    public static function getTableName() {
        return 'tasks';
    }

    public static function getTableColumns() {
        return array(
            'id',
            'name',
            'date',
            'completed',
            'depth',
            'project_id',
            'task_id',
            'user_id'
        );
    }

    private function genId() {
         $maxId = $this->dbh->prepare("
            SELECT max(id) AS id FROM (
                (SELECT max(id) AS id from tasks) 
                UNION 
                (SELECT max(id) AS id FROM notes)
            ) AS T
        ");
        $maxId->execute();
        $maxId = $maxId->fetch(PDO::FETCH_NUM);

        if (NULL != $maxId) {
            $maxId = $maxId[0];
            $maxId++;
            $this->id = $maxId;
        } else {
            $this->id = 1;
        }
    }

    //GETTERS
    public function getId() {
        return $this->id;
    }

    public function getDepth() {
        return $this->depth;
    }

    public function save() {
        $this->genId();
        parent::save();
    }

}
